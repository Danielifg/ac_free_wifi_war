//Secret App Id
let APPID = $APPID;

//Facebook SDK init
          (function (d, s, id) {
              var js, fjs = d.getElementsByTagName(s)[0];
              if (d.getElementById(id))
                  return;
              js = d.createElement(s);
              js.id = id;
              js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.10&appId="+"$APPID";
              fjs.parentNode.insertBefore(js, fjs);
          }(document, 'script', 'facebook-jssdk'));

          var client_ip = getUrlParameter("ip");
          var client_mac = getUrlParameter("mac");
          var requested_url = getUrlParameter("requested_url");
          var plan = $('#plan').val();



          // Get URL parameters for Gateway's processor
          function getUrlParameter(sParam) {
              var sPageURL = decodeURIComponent(window.location.search.substring(1)),
                        sURLVariables = sPageURL.split('&'),
                        sParameterName,
                        i;

              for (i = 0; i < sURLVariables.length; i++) {
                  sParameterName = sURLVariables[i].split('=');

                  if (sParameterName[0] === sParam) {
                      return sParameterName[1] === undefined ? true : sParameterName[1];
                  }
              }
          }
          


          window.fbAsyncInit = function () {
              FB.init({
                  appId: '$APPID',
                  autoLogAppEvents: true,
                  xfbml: true,
                  version: 'v2.9'
              });
              FB.Event.subscribe('auth.login', function () {
                  FB.api('/me', 'GET', {
                      fields: 'id,first_name,last_name,email,birthday,link,gender,locale,picture,timezone,verified,updated_time'},
                            function (fb) {
// User Object Posted to DB
                                let user = fb.getFields;

//Successful login post data to java server
                                $.ajax({
                                    headers: {
                                        'Content-Type': 'application/json'
                                    },
                                    crossDomain: true,
                                    dataType: "json",
                                    type: "POST",
                                    url: "webresources/facebook",
                                    data: JSON.stringify(user),
                                    success: function (data, textStatus, jqXHR) {
                                        if (jqXHR.status === 200) {
                                            console.log("Facebook data POST to db Succed");
                                           
                                            if (plan === 'cc') {
                                                window.location.href = 'http://ezxcess.antlabs.com/login/main.ant?c=cc';
                                            } else {
                                                window.location.href = 'http://ezxcess.antlabs.com/login/main.ant?c=proc';
                                            }
                                        } else {
                                            console.log("Facebook User not yet authenticated");
                                            console.log("textStatus = " + textStatus);
                                            console.log("jqXHR = " + jqXHR);
                                            console.log("jqXHR.status = " + jqXHR.status);
                                        }
                                    }, error: function (data, textStatus, jqXHR) {
                                        console.log("ajax call failed");
                                        console.log("textStatus = " + textStatus);
                                        console.log("jqXHR = " + jqXHR);
                                        console.log("jqXHR.status = " + jqXHR.status);
                                    }
                                });

                                function onFailure(error) {
                                    console.log(error);
                                }
                            });
              });
          };

//Log out user
          $('#logout').click(function () {

              FB.logout(function (response) {
                  console.log('FacebookUser signed out.' + response);
              });
          });




