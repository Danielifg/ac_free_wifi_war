
          $("document").ready(function () {


              var client_ip = getUrlParameter("ip");
              var client_mac = getUrlParameter("mac");
              var requested_url = getUrlParameter("requested_url");
              $('.loading-cover').hide();
              $('.hexa-loader').hide();
              $('#email_msg_window').hide();
              var error_Username = false;
              var error_Email = false;
              var error_Age = false;
              var error_Plan = false;
              var error_Terms = false;
              $("#user_name").focusout(function () {
                  check_username();
              });
              $("#email").focus(function () {
//                  $('#email_msg_window').show();
              });
              $("#email").focusout(function () {
                  check_email();
                  $('#email_msg_window').hide();
              });
              $("#select-choice-age").focusout(function () {
                  select_choice_age();
              });
              $("#plan").focusout(function () {
                  check_plan();
              });
              function check_username() {
                  var username_length = $("#user_name").val().length;
                  if (username_length < 5 || username_length === 0) {
                      $('#user_name').css({
                          border: 'solid 1px red',
                          backgroundColor: '#ff9999'
                      });
                      error_Username = true;
                  } else {
                      $('#user_name').css({
                          border: 'solid 1px #7ab5d3 ',
                          backgroundColor: '#fff'
                      });
                  }
              }

              function check_email() {
                  var email = $("#email").val();
                  var emailPattern = new RegExp(/^[+a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/i);
                  if (emailPattern.test(email)) {
                      $('#email').css({
                          border: 'solid 1px #7ab5d3',
                          backgroundColor: '#fff'
                      });
                  } else {
                      $('#email').css({
                          border: 'solid 1px red ',
                          backgroundColor: '#ff9999'

                      });
                      error_Email = true;
                  }
              }

              function select_choice_age() {
                  var age = $("#select-choice-age").val();
                  if (age === "null") {
                      $('#select-choice-age').css({
                          border: 'solid 1px red',
                          backgroundColor: '#ff9999'
                      });
                      error_Age = true;
                  } else {
                      $('#select-choice-age').css({
                          border: 'solid 1px #7ab5d3 ',
                          backgroundColor: 'white'
                      });
                  }
              }

              function check_plan() {
                  var plan = $('#plan').val();
                  if (plan === "null") {
                      $('#plan').css({
                          border: 'solid 1px red',
                          backgroundColor: '#ff9999'
                      });
                      error_Plan = true;
                  } else {
                      $('#plan').css({
                          border: 'solid 1px #7ab5d3 ',
                          backgroundColor: 'white'
                      });
                  }
              }

              function check_terms() {
                  if ($('input[type="checkbox"]').prop("checked") === false) {
                      $('#terms-label').css({
                          color: '#ff9999'
                      });
                      error_Terms = true;
                  } else if ($('input[type="checkbox"]').prop("checked") === true) {
                      $('#terms-label').css({
                          color: 'white'
                      });
                  }
              }
              // **************** Getting Gateway Initial Redirect parameters at URL **************
              function getUrlParameter(sParam) {
                  var sPageURL = decodeURIComponent(window.location.search.substring(1)),
                            sURLVariables = sPageURL.split('&'),
                            sParameterName,
                            i;
                  for (i = 0; i < sURLVariables.length; i++) {
                      sParameterName = sURLVariables[i].split('=');
                      if (sParameterName[0] === sParam) {
                          return sParameterName[1] === undefined ? true : sParameterName[1];
                      }
                  }
              }
              ;
              $("#connectBtn-mobile,#connectBtn").click(function () {
                  var name = $("#user_name").val();
                  var email = $("#email").val();
                  var age = $("#select-choice-age").val();
                  var plan = $('#plan').val();
                  error_Username = false;
                  error_Email = false;
                  error_Age = false;
                  error_Plan = false;
                  error_Terms = false;
                  check_username();
                  check_email();
                  select_choice_age();
                  check_terms();
                  check_plan();

                  if (error_Username === false && error_Email === false
                            && error_Age === false && error_Plan === false
                            && error_Terms === false) {

                      var user = new Object();
                      user.macAddress = client_mac;
                      user.ip = client_ip;
                      user.username = name;
                      user.email = email;
                      user.age = age;
                      user.plan = plan;
                      user.requestedUrl = requested_url;



//                              ** ** ** ** ** ** ** ** ** AJAX to Mail Validation ** ** ** ** ** ** ** ** ** **

//                              ** ** ** ** ** ** ** ** ** AJAX to DB ** ** ** ** ** ** ** ** ** ** ** *  *  *  ** 
                      $.ajax({
                          headers: {
                              'Content-Type': 'application/json'
                          },
                             crossDomain: true,
                          dataType: "json",
                          type: "POST",
                          url: "webresources/form",
                          data: JSON.stringify(user),
                                  beforeSend: function () {
                                      $('.hexa-loader').show();
                                      $('.loading-cover').show();
                                  }
                                  ,
                          success: function (data, textStatus, jqXHR) {
            
                              $('.hexa-loader').hide();
                              $('.loading-cover').hide();
                              if (jqXHR.status === 200) {
                                  alert("Are you ready ?");
                                  console.log("Form data POST to db Succed");
                                  if (plan === 'cc') {
                                      window.location.href = 'http://ezxcess.antlabs.com/login/main.ant?c=cc';
                                  } else {
                                      window.location.href = 'http://ezxcess.antlabs.com/login/main.ant?c=proc';
                                  }
                              } else {

                                  $('#email_msg_window').show();
                                  $('#email_msg_window').text('Please provide a valid Email').css('color', 'white');
                                  $('#email_msg_window').css({
                                      background: '#FFC107',
                                      fontColor: 'white',
                                      fontSize: '15px',
                                      fontWeight: '200'
                                  });
                                  $('#email').focus(function () {
                                      $('#email_msg_window').hide();
                                  });
                              }
                          }, error: function (data, textStatus, jqXHR) {
                              console.log("ajax call failed");
                              console.log("textStatus = " + textStatus);
                              console.log("jqXHR = " + jqXHR);
                              console.log("jqXHR.status = " + jqXHR.status);
                          }
                      });
                      function onFailure(error) {
                          console.log(error);
                      }



// **************** Posting data to Gateway Built-in Login Processor****************

//                      function postToGateway() {
//                          $.ajax({
//                              dataType: 'json',
//                              type: 'POST',
//                              contentType: 'application/json',
//                              url: 'http://ezxcess.antlabs.com/login/main.ant?c=proc&code=' + authCode
//
//                          });
//                      }


// ** ** ** ** ** ** ** ** ** **   WEb socket Connection  **  ** *** * **   *** ** * ** ** ** ** ** ** ** * *
// 
//var webSocket_form = new WebSocket("ws://localhost:51480/AC_FREE_WIFI/formSocket");
//                              console.log("socket called");
//                              sendMessage();
//                              webSocket_form.onopen = function (message) {
//                                  processOpen(message);
//                              };
//                              webSocket_form.onmessage = function (message) {
//                                  processMessage(message);
//                              };
//                              webSocket_form.onclose = function (message) {
//
//                              };
//                              webSocket_form.onerror = function (message) {
//                                  processError(message);
//                              };
//                              function processOpen(message) {
//                                  console.log("Server Connectttt");
//                              }
//                              function processMessage(message) {
//                                  console.log("received from server ==> " + message.data);
//                                  processClose();
//                                  webSocket_form.close();
//                              }
//
//                              function processClose(message) {
//                                  webSocket_form.send("Client Disconnected....");
//                                  console.log("Form endpoint Disconnected");
//                              }
//                              function processError(message) {
//                                  console.log("Error" + message);
//                              }
//
//                              function sendMessage() {
//
//                                  webSocket_form.send(JSON.stringify(user));
//                                  console.log("sent", JSON.stringify(user));
//                              }
//                              webSocket_form.close();


                  } else {
                      return null;
                  }
//                  var re = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;

              });
              $(function () {
                  $('.button-checkbox').each(function () {

                      // Settings
                      var $widget = $(this),
                                $button = $widget.find('button'),
                                $checkbox = $widget.find('input:checkbox'),
                                color = $button.data('color'),
                                settings = {
                                    on: {
                                        icon: 'glyphicon glyphicon-check'
                                    },
                                    off: {
                                        icon: 'glyphicon glyphicon-unchecked'
                                    }
                                };
                      // Event Handlers
                      $button.on('click', function () {
                          $checkbox.prop('checked', !$checkbox.is(':checked'));
                          $checkbox.triggerHandler('change');
                          updateDisplay();
                      });
                      $checkbox.on('change', function () {
                          updateDisplay();
                      });
                      // Actions
                      function updateDisplay() {
                          var isChecked = $checkbox.is(':checked');
                          // Set the button's state
                          $button.data('state', (isChecked) ? "on" : "off");
                          // Set the button's icon
                          $button.find('.state-icon')
                                    .removeClass()
                                    .addClass('state-icon ' + settings[$button.data('state')].icon);
                          // Update the button's color
                          if (isChecked) {
                              $button
                                        .removeClass('btn-default')
                                        .addClass('btn-' + color + ' active');
                          } else {
                              $button
                                        .removeClass('btn-' + color + ' active')
                                        .addClass('btn-default');
                          }
                      }
                      // Initialization
                      function init() {
                          updateDisplay();
                          // Inject the icon if applicable
                          if ($button.find('.state-icon').length === 0) {
                              $button.prepend('<i class="state-icon ' + settings[$button.data('state')].icon + '"></i>Â ');
                          }
                      }
                      init();
                  });
              });
          });
