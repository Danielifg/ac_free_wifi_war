
          $('#logout').click(function () {
              FB.logout(function (response) {
                  console.log('FacebookUser signed out.');
              });
              var auth2 = gapi.auth2.getAuthInstance();
              auth2.signOut().then(function () {
                  console.log('GoogleUser signed out.');
              });
          });

          var client_ip = getUrlParameter("ip");
          var client_mac = getUrlParameter("mac");
          var requested_url = getUrlParameter("requested_url");
          var plan = $('#plan').val();

          // **************** Getting Gateway Initial Redirect parameters at URL **************
          function getUrlParameter(sParam) {
              var sPageURL = decodeURIComponent(window.location.search.substring(1)),
                        sURLVariables = sPageURL.split('&'),
                        sParameterName,
                        i;

              for (i = 0; i < sURLVariables.length; i++) {
                  sParameterName = sURLVariables[i].split('=');

                  if (sParameterName[0] === sParam) {
                      return sParameterName[1] === undefined ? true : sParameterName[1];
                  }
              }
          }
          ;




          (function (i, s, o, g, r, a, m) {
              i['GoogleAnalyticsObject'] = r;
              i[r] = i[r] || function () {
                  (i[r].q = i[r].q || []).push(arguments);
              }, i[r].l = 1 * new Date();
              a = s.createElement(o),
                        m = s.getElementsByTagName(o)[0];
              a.async = 1;
              a.src = g;
              m.parentNode.insertBefore(a, m);
          })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

          ga('create', 'UA-102948245-1', 'auto');
          ga('send', 'pageview');
          ga('set', 'userId', '<profile.getId>'); // Set the user ID using signed-in user_id.


//              var auth2; // The Sign-In object.
//              var googleUser; // The current user.
//              var appStart = function () {
//                  gapi.load('auth2', initSigninV2);
//              };
//              var initSigninV2 = function () {
//                  auth2 = gapi.auth2.init({
//                      client_id: 'FshnR_ndXWSajKbUnm8Dw89n',
//                      scope: 'plus.profile.email.read'
//                  });
//              };
//          var webSocket_google = new WebSocket("ws://localhost:51480/AC_FREE_WIFI/googleSocket");
          function onSuccess(googleUser) {

               console.log('Logged in as: ' + googleUser.getBasicProfile().getName());
              var profile = googleUser.getBasicProfile();
              var id_token = googleUser.getAuthResponse().id_token;
              console.log(googleUser);
              for (var i = 0; i < googleUser.length; i++) {
                  console.log(googleUser[i]);
              }

              //  console.log('Signed in: ' + googleUser.getBasicProfile().getName());
              //   console.log(id_token);
              console.log('ID: ' + profile.getId());
              console.log('Full Name: ' + profile.getName());
              console.log('Given Name: ' + profile.getGivenName());
              console.log('Family Name: ' + profile.getFamilyName());
              console.log('Image URL: ' + profile.getImageUrl());
              console.log('Email: ' + profile.getEmail());


              var user = new Object();
              user.googleid = profile.getId();
              user.fullname = profile.getName();
              user.givenname = profile.getGivenName();
              user.familyname = profile.getFamilyName();
              user.macAddress = client_mac;
              user.ip = client_ip;
              user.plan = plan;
              user.requestedUrl = requested_url;
              user.imageurl = profile.getImageUrl();
              user.email = profile.getEmail();

//*************   WEBSOCKETS ************   

              $.ajax({
                  headers: {
                      'Content-Type': 'application/json'
                  },
                  crossDomain: true,
                  dataType: "json",
                  type: "POST",
                  url: "webresources/google",
                  data: JSON.stringify(user),
                  success: function (data, textStatus, jqXHR) {
                      if (jqXHR.status === 200) {
                          console.log("GOOGLE data POST to db Succed");
                          alert("Are you ready ?");
                          if (plan === 'cc') {
                              window.location.href = 'http://ezxcess.antlabs.com/login/main.ant?c=cc';
                          } else {
                              window.location.href = 'http://ezxcess.antlabs.com/login/main.ant?c=proc';
                          }
                      } else {
                          console.log("Form User not yet authenticated");
                      }
                  }, error: function (data, textStatus, jqXHR) {
                      console.log("ajax call failed");
                      console.log("textStatus = " + textStatus);
                      console.log("jqXHR = " + jqXHR);
                      console.log("jqXHR.status = " + jqXHR.status);
                  }
              });

//                  sendMessage();
//                  function sendMessage(closeTCP) {
//                      webSocket_google.send(JSON.stringify(user));
//                      console.log("sent", JSON.stringify(user));
//                      
//                  }                                 
//
//                  webSocket_google.onopen = function (message) {
//                      processOpen(message);
//                  };
//                  webSocket_google.onmessage = function (message) {
//                      processMessage(message);
//                  };
//                  webSocket_google.onclose = function (message) {
//                      processClose(message);                
//                  };
//                  webSocket_google.onerror = function (message) {
//                      processError(message);
//                  };
//
//                  function processOpen(message) {
//                      console.log("Server Connectttt");
//                  }
//                  function processMessage(message) {
//                      console.log("received from server ==> " + message.data);       
//                      webSocket_google.close();
//                  }
//
//                  function processClose(message) {
//                      webSocket_google.send("Client Disconnected....");
//                      console.log("Google Endpoint Disconnected");
//                  }
//                  function processError(message) {
//                      console.log("Error" + message);
//                  }

          }

//              
//              setTimeout(function() { webSocket.close(); }, 5000);

          function onFailure(error) {
              console.log(error);
          }
          function renderButton() {
              gapi.signin2.render('my-signin2', {
                  'scope': 'profile email',
                  'width': 235,
                  'height': 40,
                  'longtitle': true,
                  'theme': 'dark',
                  'onsuccess': onSuccess,
                  'onfailure': onFailure
              });
          }




      