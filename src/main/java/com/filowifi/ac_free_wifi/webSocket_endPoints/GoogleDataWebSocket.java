///*
// * To change this license header, choose License Headers in Project Properties.
// * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// */
//package com.filowifi.ac_free_wifi.webSocket_endPoints;
//
//import com.fasterxml.jackson.databind.ObjectMapper;
//import com.filowifi.ac_free_wifi.controller.jpaControl.GoogleJpaController;
//import com.filowifi.ac_free_wifi.entities.Google;
//import java.io.Serializable;
//import java.util.logging.Level;
//import java.util.logging.Logger;
//import javax.enterprise.context.SessionScoped;
//import javax.inject.Inject;
//import javax.websocket.OnClose;
//import javax.websocket.OnError;
//import javax.websocket.OnMessage;
//import javax.websocket.OnOpen;
//import javax.websocket.server.ServerEndpoint;
//
///**
// *
// * @author Daniel
// */
//@SessionScoped
//@ServerEndpoint("/googleSocket")
//public class GoogleDataWebSocket implements Serializable {
//
//        private static final Logger LOG = Logger.getLogger(GoogleDataWebSocket.class.getName());
//
//        @Inject
//        private GoogleJpaController jpa;
//
//        @Inject
//        private Google google;
//
//        @OnOpen
//        public void handleOpen() {
//                LOG.log(Level.INFO, String.format("Google Handle Open"));
//
//        }
//
//        @OnMessage
//        public String handleMessage(String message) {
//                LOG.log(Level.INFO, String.format("Google Handle Message" + message));
//                try {
//                        ObjectMapper mapper = new ObjectMapper();
//                        
//                        google = mapper.readValue(message, Google.class);
//                        google.setSysid(Integer.SIZE);
//                        jpa.create(google);
//                        LOG.log(Level.INFO, "JPA GOOGLE  DATA HITTING DB");
//                } catch (Exception ex) {
//                        LOG.log(Level.WARNING, "JPA GOOGLE  DATA HITTING DB ===> FAIL");
//                        LOG.log(Level.WARNING, google.toString());
//                        Logger.getLogger(GoogleDataWebSocket.class.getName()).log(Level.SEVERE, null, ex);
//                }
//                String antlabsWalledGardenRedirect="window.location.replace(\"http://antlabs.com\")";
//                return antlabsWalledGardenRedirect;
//        }
//
//        @OnClose
//        public void handleClose() {               
//                LOG.log(Level.INFO, String.format("Google Handle Close()"));
//        }
//
//        @OnError
//        public void handleEror(Throwable t) {
//                t.printStackTrace();
//        }
//
//}
