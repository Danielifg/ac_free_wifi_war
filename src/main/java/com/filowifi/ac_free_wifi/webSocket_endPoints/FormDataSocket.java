///*
// * To change this license header, choose License Headers in Project Properties.
// * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// */
//package com.filowifi.ac_free_wifi.webSocket_endPoints;
//
//import com.fasterxml.jackson.databind.ObjectMapper;
//import com.filowifi.ac_free_wifi.controller.jpaControl.FormdataJpaController;
//import com.filowifi.ac_free_wifi.entities.Formdata;
//import java.io.Serializable;
//import java.util.logging.Level;
//import java.util.logging.Logger;
//import javax.enterprise.context.SessionScoped;
//import javax.inject.Inject;
//import javax.websocket.OnClose;
//import javax.websocket.OnError;
//import javax.websocket.OnMessage;
//import javax.websocket.OnOpen;
//import javax.websocket.server.ServerEndpoint;
//
///**
// *
// * @author Daniel
// */
//@SessionScoped
//@ServerEndpoint("/formSocket")
//public class FormDataSocket implements Serializable {
//
//        @Inject
//        private FormdataJpaController jpa;
//
//        @Inject
//        private Formdata formdata;
//
//        private static final Logger LOG = Logger.getLogger(FormDataSocket.class.getName());                
//
//        @OnOpen
//        public void handleOpen() {
//                LOG.log(Level.INFO, String.format("Form Connection Open"));
//              
//        }
//        
//        @OnMessage
//        public String handleMessage(String message) {                
//                LOG.log(Level.INFO, String.format("Form Handling Message"+message));
//                try {
//                        ObjectMapper mapper = new ObjectMapper();
//                        
//                        formdata = mapper.readValue(message, Formdata.class);
//                        formdata.setSysid(Integer.SIZE);                                    
//                        jpa.create(formdata);
//                        LOG.log(Level.INFO, "JPA FORM  DATA HITTING DB");
//                } catch (Exception ex) {
//                        LOG.log(Level.WARNING, "JPA FORM  DATA HITTING DB ===> FAIL");
//                        Logger.getLogger(FormDataSocket.class.getName()).log(Level.SEVERE, null, ex);
//                }
//                return message;
//        }
//
//        @OnClose
//        public void handleClose() {
//                LOG.log(Level.INFO, String.format("Form Handling Close"));
//        }
//
//        @OnError
//        public void handleEror(Throwable t) {
//                t.printStackTrace();
//        }
//}
