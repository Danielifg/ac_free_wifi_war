///*
// * To change this license header, choose License Headers in Project Properties.
// * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// */
//package com.filowifi.ac_free_wifi.webSocket_endPoints;
//
//import com.fasterxml.jackson.databind.ObjectMapper;
//import com.filowifi.ac_free_wifi.controller.jpaControl.FacebookJpaController;
//import com.filowifi.ac_free_wifi.entities.Facebook;
//import java.io.Serializable;
//import java.io.StringReader;
//import java.util.logging.Level;
//import java.util.logging.Logger;
//import javax.enterprise.context.SessionScoped;
//import javax.inject.Inject;
//import javax.json.Json;
//import javax.json.JsonArray;
//import javax.json.JsonObject;
//import javax.json.JsonReader;
//import javax.json.JsonValue;
//import javax.websocket.OnClose;
//import javax.websocket.OnError;
//import javax.websocket.OnMessage;
//import javax.websocket.OnOpen;
//import javax.websocket.server.ServerEndpoint;
//
///**
// *
// * @author Daniel
// *
// */
//@SessionScoped
//@ServerEndpoint("/facebookSocket")
//public class FacebookDataSocket implements Serializable {
//
//        @Inject
//        private FacebookJpaController jpa;
//
//        @Inject
//        private Facebook facebook;
//
//        private static final Logger LOG = Logger.getLogger(FacebookDataSocket.class.getName());
//
//        @OnOpen
//        public void handleOpen() {
//                LOG.log(Level.INFO, String.format("Facebook Connection Open"));
//                System.out.println("Conection Open");
//        }
//
//        @OnMessage
//        public String handleMessage(String message) {
//                LOG.log(Level.INFO, String.format("Facebook Handling Message"));
//                try {
//                        ObjectMapper mapper = new ObjectMapper();
//                        facebook = mapper.readValue(message, Facebook.class);
//                        facebook.setSysid(Integer.SIZE);
//                        jpa.create(facebook);
//                        LOG.log(Level.INFO, "JPA FACEBOOK  DATA HITTING DB");
//                } catch (Exception ex) {
//                        LOG.log(Level.WARNING, "JPA FACEBOOK  DATA HITTING DB ===> FAIL");
//                        Logger.getLogger(FormDataSocket.class.getName()).log(Level.SEVERE, null, ex);
//                }
//                return message;
//        }
//
//        @OnClose
//        public void handleClose() {
//                System.out.println("Data Received");
//                LOG.log(Level.INFO, String.format("Facebook Handling Close()"));
//        }
//
//        @OnError
//        public void handleEror(Throwable t) {
//                t.printStackTrace();
//        }

//        private Facebook jsonToUser(String userJson) {
//                
//                facebook.setSysid(1);
//                
//              
//
//                JsonArray jsonArray;
//                try (JsonReader jsonReader = Json.createReader(new StringReader(userJson))) {
//                        jsonArray = jsonReader.readArray();
//                }
//
//                for (JsonValue jsonValue : jsonArray) {
//                        JsonObject jsonObject = (JsonObject) jsonValue;
//                        String propertyName = jsonObject.getString("name");
//                        String propertyValue = jsonObject.getString("value");
//
//                        switch (propertyName) {
//                                case "facebookId":
//                                        facebook.setFacebookid(propertyValue);
//                                        break;
//                                case "firstName":
//                                        facebook.setFirstname(propertyValue);
//                                        break;
//                                case "lastName":
//                                        facebook.setLastname(propertyValue);
//                                        break;
//                                case "gender":
//                                        facebook.setGender(propertyValue);
//                                        break;
//                                case "birthday":
//                                        facebook.setBirthday(propertyValue);
//                                        break;
//                                case "link":
//                                        facebook.setAccountlink(propertyValue);
//                                        break;
//                                case "locale":
//                                        facebook.setLocale(propertyValue);
//                                        break;
//                                case "url":
//                                        facebook.setUrl(propertyValue);
//                                        break;
//                                case "updated_time":
//                                        facebook.setUpdatedtime(propertyValue);
//                                        break;
//                                case "verified":
//                                        facebook.setVerified(propertyValue);
//                                        break;                          
//                                default:
//                                        LOG.log(Level.WARNING, String.format("Unknown property name found: %s", propertyName));
//                                        break;
//                        }
//                }
//                return facebook;
//        }
//}
