/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.filowifi.ac_free_wifi.util;

/*
 *******************************************************************************************
 * Company:
 * © 2017, Email Hippo Limited. (http://emailhippo.com)
 *
 * File name:
 * API V3 - Java Example.java
 *
 * Version:
 * 1.0.20170508.0
 *
 * Version control:
 * - 1.0.20170508.0 - initial release
 *
 * Date:
 * May 2017
 *
 * Description:
 * Demonstrates how to call a RESTful service @ //api.hippoapi.com/v3/more/json
 * using java.
 *
 * This example requires a valid key to work correctly.
 *
 * License:
 * Apache 2.0 (https://www.apache.org/licenses/LICENSE-2.0)
 *******************************************************************************************
 */
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.filowifi.ac_free_wifi.http_Service.FormData_Client;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;

public class EmailValidation {
        private static final Logger LOG = Logger.getLogger(FormData_Client.class.getName());
        /**
         * The main program entry point
         *
         * @param email
         * @param args the command line arguments
         * @return  boolean validEmail
         * @throws IOException If the server does not return a success response
         */
        public boolean validEmail(String email) {
                
      String ApiUrl = "https://api.hippoapi.com/v3/more/json";
      String QueryFormatString = ApiUrl + "/2A9A3C4B/" + email;
//    john@filowifi                coorporate key 886844AF    (5K requests)
//    danielf@filowifi.com   development key 2A9A3C4B   (100 requests)
         String YourAPIKey = "/2A9A3C4B";
        boolean valid = false;

        
                try {
                        // Format the request url to the correct structure for the request
                        URL requestUrl = new URL(String.format(QueryFormatString, ApiUrl, YourAPIKey, email));

                // Open a connection to the website
                HttpURLConnection myRequest = (HttpURLConnection) requestUrl.openConnection();

                // Set the type to HTTP GET
                myRequest.setRequestMethod("GET");

                // Create a new buffered reader to read the response back from the server
                BufferedReader reader = new BufferedReader(
                        new InputStreamReader(myRequest.getInputStream()));

                String inputLine;
                StringBuilder response = new StringBuilder();

                // Read in the response line from the server
                while ((inputLine = reader.readLine()) != null) {
                        response.append(inputLine);
                }

                // Close the reader
                reader.close();

                //create ObjectMapper instance
                ObjectMapper objectMapper = new ObjectMapper();

                JsonNode rootNode = objectMapper.readTree(response.toString());
                JsonNode sendAssessNode = rootNode.path("sendAssess");
                JsonNode inboxQualityScore = sendAssessNode.path("inboxQualityScore");
                System.out.println(email);
                System.out.println("assesNode: " + sendAssessNode);
                System.out.println("input quelit score : " + inboxQualityScore);
                System.out.println("int score : " + inboxQualityScore.asInt());
                System.out.println(requestUrl);

                if (inboxQualityScore.asInt() >= 5) {
                        valid = true;
                        LOG.log(Level.INFO, "score {0}", inboxQualityScore.asInt());
                }
        }
        catch (IOException ex

        
                ) {
                        Logger.getLogger(EmailValidation.class.getName()).log(Level.SEVERE, null, ex);
        }
        return valid ;
}

}
