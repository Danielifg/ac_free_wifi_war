/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.filowifi.ac_free_wifi.http_Service;

import com.google.appengine.repackaged.com.google.api.client.extensions.appengine.http.UrlFetchTransport;
import com.google.appengine.repackaged.com.google.api.client.googleapis.auth.oauth2.GoogleIdToken;
import com.google.appengine.repackaged.com.google.api.client.googleapis.auth.oauth2.GoogleIdToken.Payload;
import com.google.appengine.repackaged.com.google.api.client.googleapis.auth.oauth2.GoogleIdTokenVerifier;
import com.google.appengine.repackaged.com.google.api.client.json.jackson2.JacksonFactory;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.filowifi.ac_free_wifi.controller.jpaControl.GoogleJpaController;
import com.filowifi.ac_free_wifi.entities.Google;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.Collections;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.inject.Inject;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * REST Web Service
 *
 * @author Daniel
 */
@Path("/google")
public class GoogleData_Client {

        private static final Logger LOG = Logger.getLogger(GoogleData_Client.class.getName());
        private static final JacksonFactory JACKSONFACTORY = new JacksonFactory();
        private static String userData;
        private static String CLIENT_ID;

        @Context
        private UriInfo context;

        @Inject
        private GoogleJpaController jpa;

        @Inject
        private Google google;

        /**
         * Creates a new instance of GoogleData_Client
         */
        public GoogleData_Client() {
        }

        @POST
        @Consumes(MediaType.APPLICATION_JSON)
        @Produces(MediaType.APPLICATION_JSON)
        public Response getJson(String googleUserData) throws GeneralSecurityException, IOException, Exception {
                Response response;
                userData = googleUserData;

                try {
                        ObjectMapper mapper = new ObjectMapper();
                        google = mapper.readValue(googleUserData, Google.class);
                        google.setSysid(Integer.SIZE);
                        jpa.create(google);
                        LOG.log(Level.INFO, "JPA GOOGLE  DATA HITTING DB ===> SUCCED");
                } catch (JsonProcessingException ex) {
                        LOG.log(Level.WARNING, "JPA GOOGLE  DATA HITTING DB ===> FAIL");
                        LOG.log(Level.WARNING, google.toString());
                        Logger.getLogger(GoogleData_Client.class.getName()).log(Level.SEVERE, null, ex);
                }

                return Response.status(200).entity(userData).build();
//
//                GoogleIdTokenVerifier verifier = new GoogleIdTokenVerifier.Builder(UrlFetchTransport.getDefaultInstance(), JACKSONFACTORY)
//                        .setAudience(Collections.singletonList(CLIENT_ID))
//                        // Or, if multiple clients access the backend:
//                        //.setAudience(Arrays.asList(CLIENT_ID_1, CLIENT_ID_2, CLIENT_ID_3))
//                        .build();
//
//                GoogleIdToken idToken = verifier.verify(userData);
//                if (idToken != null) {
//                        Payload payload = idToken.getPayload();
//
//                        // Print user identifier
//                        String userId = payload.getSubject();
//                        System.out.println("User ID: " + userId);
//
//                        // Get profile information from payload
//                        String email = payload.getEmail();
//                        boolean emailVerified = Boolean.valueOf(payload.getEmailVerified());
//                        String name = (String) payload.get("name");
//                        String pictureUrl = (String) payload.get("picture");
//                        String locale = (String) payload.get("locale");
//                        String familyName = (String) payload.get("family_name");
//                        String givenName = (String) payload.get("given_name");
//                        userData = name + " " + familyName + " " + givenName;
//                        System.out.println(userData);
//                } else {
//                        System.out.println("Invalid ID token.");
//                        System.out.println(userData);
//                }

        }

}
