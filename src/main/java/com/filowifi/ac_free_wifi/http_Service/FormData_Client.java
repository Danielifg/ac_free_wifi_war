package com.filowifi.ac_free_wifi.http_Service;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.filowifi.ac_free_wifi.controller.jpaControl.FormdataJpaController;
import com.filowifi.ac_free_wifi.entities.Formdata;
import com.filowifi.ac_free_wifi.util.EmailValidation;

import java.io.Serializable;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.inject.Inject;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * REST Web Service
 *
 * @author Daniel
 */
@Path("/form")
public class FormData_Client implements Serializable {

        @Context
        private UriInfo context;

        @Inject
        private FormdataJpaController jpa;

        @Inject
        private Formdata formdata;

        private static final Logger LOG = Logger.getLogger(FormData_Client.class.getName());

        @POST
        @Consumes({MediaType.APPLICATION_JSON})
        @Produces(MediaType.APPLICATION_JSON)
        public Response getUserJson(String userJsonsg) throws Exception {
                
             int status;
                status = 0;
                try {
                        ObjectMapper mapper = new ObjectMapper();
                        formdata = mapper.readValue(userJsonsg, Formdata.class);
                        formdata.setSysid(Integer.SIZE);
                        EmailValidation ev = new EmailValidation();
                        if (ev.validEmail(formdata.getEmail()) == true) {
                                  LOG.log(Level.INFO, formdata.toString());
                                jpa.create(formdata);
                                status= 200;
                                LOG.log(Level.INFO, "JPA FORM  DATA HITTING DB ===> SUCCED");
                        } else {
                                status= 500;
                                LOG.log(Level.INFO, "Invalid Email");
                        }
                } catch (JsonProcessingException ex) {
                        LOG.log(Level.WARNING, "JPA FORM  DATA HITTING DB ===> FAIL");
                        Logger.getLogger(FormData_Client.class.getName()).log(Level.SEVERE, null, ex);
                }
                       LOG.log(Level.WARNING, status +"");
                return Response.status(status).entity(userJsonsg).build();
                
                
        }
        
        
        
        
        
        
        
        
}
