/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.filowifi.ac_free_wifi.http_Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.filowifi.ac_free_wifi.controller.jpaControl.FacebookJpaController;
import com.filowifi.ac_free_wifi.entities.Facebook;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.inject.Inject;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * REST Web Service
 *
 * @author Daniel
 */
@Path("/facebook")
public class FacebookData_Client {

        @Context
        private UriInfo context;

        @Inject
        private Facebook facebook;

        @Inject
        private FacebookJpaController jpa;

        private static final Logger LOG = Logger.getLogger(FacebookData_Client.class.getName());
        /**
         * Creates a new instance of fb
         */
        public FacebookData_Client() {
        }

        @POST
        @Consumes({MediaType.APPLICATION_JSON})
        @Produces({MediaType.APPLICATION_JSON})
        public Response postStrMsg(String message) throws Exception {                
                
                try {
                        ObjectMapper mapper = new ObjectMapper();
                        facebook = mapper.readValue(message, Facebook.class);
                        facebook.setSysid(Integer.SIZE);
                        jpa.create(facebook);
                        LOG.log(Level.INFO, "JPA FACEBOOK  DATA HITTING DB ==> SUCCED");
                } catch (JsonProcessingException ex) {
                        Logger.getLogger(FacebookData_Client.class.getName()).log(Level.SEVERE, null, ex);
                        LOG.log(Level.WARNING, "JPA FACEBOOK  DATA HITTING DB ===> FAIL");
                }
                return Response.status(200).entity(message).build();
        }

}
