/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.filowifi.ac_free_wifi.controller.jpaControl;

import com.filowifi.ac_free_wifi.entities.Facebook;
import com.filowifi.ac_free_wifi.controller.jpaControl.exceptions.NonexistentEntityException;
import com.filowifi.ac_free_wifi.controller.jpaControl.exceptions.RollbackFailureException;
import java.io.Serializable;
import java.util.List;
import javax.annotation.Resource;
import javax.enterprise.context.ApplicationScoped;

import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.transaction.UserTransaction;

/**
 *
 * @author Daniel
 */
@Named
@ApplicationScoped
public class FacebookJpaController implements Serializable {

        @Resource
        private UserTransaction utx;

        @PersistenceContext(unitName = "com.filowifi_AC_FREE_WIFI_war_1.0-SNAPSHOTPU")
        private EntityManager em;

        public void create(Facebook facebook) throws RollbackFailureException, Exception {

                try {
                        utx.begin();
                        em.persist(facebook);
                        utx.commit();
                } catch (Exception ex) {
                        try {
                                utx.rollback();
                        } catch (Exception re) {
                                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
                        }
                        throw ex;
                }
        }

        public void edit(Facebook facebook) throws NonexistentEntityException, RollbackFailureException, Exception {

                try {
                        utx.begin();

                        facebook = em.merge(facebook);
                        utx.commit();
                } catch (Exception ex) {
                        try {
                                utx.rollback();
                        } catch (Exception re) {
                                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
                        }
                        String msg = ex.getLocalizedMessage();
                        if (msg == null || msg.length() == 0) {
                                Integer id = facebook.getSysid();
                                if (findFacebook(id) == null) {
                                        throw new NonexistentEntityException("The facebook with id " + id + " no longer exists.");
                                }
                        }
                        throw ex;
                }
        }

        public void destroy(Integer id) throws NonexistentEntityException, RollbackFailureException, Exception {

                try {
                        utx.begin();

                        Facebook facebook;
                        try {
                                facebook = em.getReference(Facebook.class, id);
                                facebook.getSysid();
                        } catch (EntityNotFoundException enfe) {
                                throw new NonexistentEntityException("The facebook with id " + id + " no longer exists.", enfe);
                        }
                        em.remove(facebook);
                        utx.commit();
                } catch (Exception ex) {
                        try {
                                utx.rollback();
                        } catch (Exception re) {
                                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
                        }
                        throw ex;
                }
        }

        public List<Facebook> findFacebookEntities() {
                return findFacebookEntities(true, -1, -1);
        }

        public List<Facebook> findFacebookEntities(int maxResults, int firstResult) {
                return findFacebookEntities(false, maxResults, firstResult);
        }

        private List<Facebook> findFacebookEntities(boolean all, int maxResults, int firstResult) {

                CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
                cq.select(cq.from(Facebook.class));
                Query q = em.createQuery(cq);
                if (!all) {
                        q.setMaxResults(maxResults);
                        q.setFirstResult(firstResult);
                }
                return q.getResultList();
        }

        public Facebook findFacebook(Integer id) {
                return em.find(Facebook.class, id);
        }

        public int getFacebookCount() {
                CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
                Root<Facebook> rt = cq.from(Facebook.class);
                cq.select(em.getCriteriaBuilder().count(rt));
                Query q = em.createQuery(cq);
                return ((Long) q.getSingleResult()).intValue();

        }

}
