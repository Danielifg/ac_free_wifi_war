/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.filowifi.ac_free_wifi.controller.jpaControl;

import com.filowifi.ac_free_wifi.entities.Formdata;
import com.filowifi.ac_free_wifi.controller.jpaControl.exceptions.NonexistentEntityException;
import com.filowifi.ac_free_wifi.controller.jpaControl.exceptions.RollbackFailureException;
import java.io.Serializable;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.Resource;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;


import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.transaction.UserTransaction;

/**
 *
 * @author Daniel
 */
@Named
@ApplicationScoped
public class FormdataJpaController implements Serializable {

        private static final Logger LOG = Logger.getLogger(FormdataJpaController.class.getName());

        @Resource
        private UserTransaction utx;

        @PersistenceContext(unitName = "com.filowifi_AC_FREE_WIFI_war_1.0-SNAPSHOTPU")
        private EntityManager em;

        public void create(Formdata formdata) throws RollbackFailureException, Exception {
                try {
                        utx.begin();
                        em.persist(formdata);
                        utx.commit();
                } catch (Exception ex) {
                        LOG.log(Level.SEVERE, ex.getMessage());
                        try {
                                utx.rollback();
                        } catch (Exception re) {
                                LOG.log(Level.SEVERE, re.getMessage());
                                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
                        }
                        throw ex;
                }
        }

        public void edit(Formdata formdata) throws NonexistentEntityException, RollbackFailureException, Exception {

                try {
                        utx.begin();
                        formdata = em.merge(formdata);
                        utx.commit();
                } catch (Exception ex) {
                        try {
                                utx.rollback();
                        } catch (Exception re) {
                                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
                        }
                        String msg = ex.getLocalizedMessage();
                        if (msg == null || msg.length() == 0) {
                                Integer id = formdata.getSysid();
                                if (findFormdata(id) == null) {
                                        throw new NonexistentEntityException("The formdata with id " + id + " no longer exists.");
                                }
                        }
                        throw ex;
                }
        }

        public void destroy(Integer id) throws NonexistentEntityException, RollbackFailureException, Exception {
                try {
                        utx.begin();

                        Formdata formdata;
                        try {
                                formdata = em.getReference(Formdata.class, id);
                                formdata.getSysid();
                        } catch (EntityNotFoundException enfe) {
                                throw new NonexistentEntityException("The formdata with id " + id + " no longer exists.", enfe);
                        }
                        em.remove(formdata);
                        utx.commit();
                } catch (Exception ex) {
                        try {
                                utx.rollback();
                        } catch (Exception re) {
                                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
                        }
                        throw ex;
                }
        }

        public List<Formdata> findFormdataEntities() {
                return findFormdataEntities(true, -1, -1);
        }

        public List<Formdata> findFormdataEntities(int maxResults, int firstResult) {
                return findFormdataEntities(false, maxResults, firstResult);
        }

        private List<Formdata> findFormdataEntities(boolean all, int maxResults, int firstResult) {

                CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
                cq.select(cq.from(Formdata.class));
                Query q = em.createQuery(cq);
                if (!all) {
                        q.setMaxResults(maxResults);
                        q.setFirstResult(firstResult);
                }
                return q.getResultList();

        }

        public Formdata findFormdata(Integer id) {
                return em.find(Formdata.class, id);

        }

        public int getFormdataCount() {

                CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
                Root<Formdata> rt = cq.from(Formdata.class);
                cq.select(em.getCriteriaBuilder().count(rt));
                Query q = em.createQuery(cq);
                return ((Long) q.getSingleResult()).intValue();
        }

}
