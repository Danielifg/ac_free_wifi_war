/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.filowifi.ac_free_wifi.controller.jpaControl;

import com.filowifi.ac_free_wifi.entities.Admins;
import com.filowifi.ac_free_wifi.controller.jpaControl.exceptions.NonexistentEntityException;
import com.filowifi.ac_free_wifi.controller.jpaControl.exceptions.RollbackFailureException;
import java.io.Serializable;
import java.util.List;
import javax.annotation.Resource;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.transaction.UserTransaction;

/**
 *
 * @author Daniel
 */
@Named
@RequestScoped
public class AdminsJpaController implements Serializable {

        @Resource
        private UserTransaction utx;

        @PersistenceContext(unitName = "com.filowifi_AC_FREE_WIFI_war_1.0-SNAPSHOTPU")
        private EntityManager em;

        public void create(Admins admins) throws RollbackFailureException, Exception {
             
                try {
                        utx.begin();
                        em.persist(admins);
                        utx.commit();
                } catch (Exception ex) {
                        try {
                                utx.rollback();
                        } catch (Exception re) {
                                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
                        }
                        throw ex;
                }  
        }

        public void edit(Admins admins) throws NonexistentEntityException, RollbackFailureException, Exception {
                
                try {
                        utx.begin();
                 
                        admins = em.merge(admins);
                        utx.commit();
                } catch (Exception ex) {
                        try {
                                utx.rollback();
                        } catch (Exception re) {
                                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
                        }
                        String msg = ex.getLocalizedMessage();
                        if (msg == null || msg.length() == 0) {
                                Integer id = admins.getId();
                                if (findAdmins(id) == null) {
                                        throw new NonexistentEntityException("The admins with id " + id + " no longer exists.");
                                }
                        }
                        throw ex;
                } 
        }

        public void destroy(Integer id) throws NonexistentEntityException, RollbackFailureException, Exception {
             
                try {
                        utx.begin();
                  
                        Admins admins;
                        try {
                                admins = em.getReference(Admins.class, id);
                                admins.getId();
                        } catch (EntityNotFoundException enfe) {
                                throw new NonexistentEntityException("The admins with id " + id + " no longer exists.", enfe);
                        }
                        em.remove(admins);
                        utx.commit();
                } catch (Exception ex) {
                        try {
                                utx.rollback();
                        } catch (Exception re) {
                                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
                        }
                        throw ex;
                }  
        }

        public List<Admins> findAdminsEntities() {
                return findAdminsEntities(true, -1, -1);
        }

        public List<Admins> findAdminsEntities(int maxResults, int firstResult) {
                return findAdminsEntities(false, maxResults, firstResult);
        }

        private List<Admins> findAdminsEntities(boolean all, int maxResults, int firstResult) {
 
                        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
                        cq.select(cq.from(Admins.class));
                        Query q = em.createQuery(cq);
                        if (!all) {
                                q.setMaxResults(maxResults);
                                q.setFirstResult(firstResult);
                        }
                        return q.getResultList(); 
        }

        public Admins findAdmins(Integer id) {         
                        return em.find(Admins.class, id);           
        }

        public int getAdminsCount() {
          
                
                        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
                        Root<Admins> rt = cq.from(Admins.class);
                        cq.select(em.getCriteriaBuilder().count(rt));
                        Query q = em.createQuery(cq);
                        return ((Long) q.getSingleResult()).intValue();
        }

}
