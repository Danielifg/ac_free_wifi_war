/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.filowifi.ac_free_wifi.controller.jpaControl;

import com.filowifi.ac_free_wifi.entities.Google;
import com.filowifi.ac_free_wifi.controller.jpaControl.exceptions.NonexistentEntityException;
import com.filowifi.ac_free_wifi.controller.jpaControl.exceptions.RollbackFailureException;
import java.io.Serializable;
import java.util.List;
import javax.annotation.Resource;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.transaction.UserTransaction;

/**
 *
 * @author Daniel
 */
@Named
@ApplicationScoped
public class GoogleJpaController implements Serializable {

        @Resource
        private UserTransaction utx;

        @PersistenceContext(unitName = "com.filowifi_AC_FREE_WIFI_war_1.0-SNAPSHOTPU")
        private EntityManager em;

        public void create(Google google) throws RollbackFailureException, Exception {
                try {
                        utx.begin();
                        em.persist(google);
                        utx.commit();
                } catch (Exception ex) {
                        try {
                                utx.rollback();
                        } catch (Exception re) {
                                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
                        }
                        throw ex;
                }
        }

        public void edit(Google google) throws NonexistentEntityException, RollbackFailureException, Exception {
                try {
                        utx.begin();
                        google = em.merge(google);
                        utx.commit();
                } catch (Exception ex) {
                        try {
                                utx.rollback();
                        } catch (Exception re) {
                                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
                        }
                        String msg = ex.getLocalizedMessage();
                        if (msg == null || msg.length() == 0) {
                                Integer id = google.getSysid();
                                if (findGoogle(id) == null) {
                                        throw new NonexistentEntityException("The google with id " + id + " no longer exists.");
                                }
                        }
                        throw ex;
                }
        }

        public void destroy(Integer id) throws NonexistentEntityException, RollbackFailureException, Exception {
                try {
                        utx.begin();
                        Google google;
                        try {
                                google = em.getReference(Google.class, id);
                                google.getSysid();
                        } catch (EntityNotFoundException enfe) {
                                throw new NonexistentEntityException("The google with id " + id + " no longer exists.", enfe);
                        }
                        em.remove(google);
                        utx.commit();
                } catch (Exception ex) {
                        try {
                                utx.rollback();
                        } catch (Exception re) {
                                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
                        }
                        throw ex;
                }
        }

        public List<Google> findGoogleEntities() {
                return findGoogleEntities(true, -1, -1);
        }

        public List<Google> findGoogleEntities(int maxResults, int firstResult) {
                return findGoogleEntities(false, maxResults, firstResult);
        }

        private List<Google> findGoogleEntities(boolean all, int maxResults, int firstResult) {
                CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
                cq.select(cq.from(Google.class));
                Query q = em.createQuery(cq);
                if (!all) {
                        q.setMaxResults(maxResults);
                        q.setFirstResult(firstResult);
                }
                return q.getResultList();
        }

        public Google findGoogle(Integer id) {
                return em.find(Google.class, id);
        }

        public int getGoogleCount() {
                CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
                Root<Google> rt = cq.from(Google.class);
                cq.select(em.getCriteriaBuilder().count(rt));
                Query q = em.createQuery(cq);
                return ((Long) q.getSingleResult()).intValue();
        }

}
