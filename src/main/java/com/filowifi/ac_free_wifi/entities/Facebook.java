/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.filowifi.ac_free_wifi.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author null
 */
@Entity
@Table(name = "FACEBOOK", catalog = "AC_FREE_WIFI_USERS", schema = "")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Facebook.findAll", query = "SELECT f FROM Facebook f")
    , @NamedQuery(name = "Facebook.findBySysid", query = "SELECT f FROM Facebook f WHERE f.sysid = :sysid")
    , @NamedQuery(name = "Facebook.findByFacebookid", query = "SELECT f FROM Facebook f WHERE f.facebookid = :facebookid")
    , @NamedQuery(name = "Facebook.findByFirstname", query = "SELECT f FROM Facebook f WHERE f.firstname = :firstname")
    , @NamedQuery(name = "Facebook.findByLastname", query = "SELECT f FROM Facebook f WHERE f.lastname = :lastname")
    , @NamedQuery(name = "Facebook.findByGender", query = "SELECT f FROM Facebook f WHERE f.gender = :gender")
    , @NamedQuery(name = "Facebook.findByBirthday", query = "SELECT f FROM Facebook f WHERE f.birthday = :birthday")
    , @NamedQuery(name = "Facebook.findByAccountlink", query = "SELECT f FROM Facebook f WHERE f.accountlink = :accountlink")
    , @NamedQuery(name = "Facebook.findByLocale", query = "SELECT f FROM Facebook f WHERE f.locale = :locale")
    , @NamedQuery(name = "Facebook.findByUrl", query = "SELECT f FROM Facebook f WHERE f.url = :url")
    , @NamedQuery(name = "Facebook.findByUpdatedtime", query = "SELECT f FROM Facebook f WHERE f.updatedtime = :updatedtime")
    , @NamedQuery(name = "Facebook.findByVerified", query = "SELECT f FROM Facebook f WHERE f.verified = :verified")})
public class Facebook implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "SYSID", nullable = false)
    private Integer sysid;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "FACEBOOKID", nullable = false, length = 50)
    private String facebookid;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "FIRSTNAME", nullable = false, length = 30)
    private String firstname;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "LASTNAME", nullable = false, length = 30)
    private String lastname;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 16)
    @Column(name = "GENDER", nullable = false, length = 16)
    private String gender;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "BIRTHDAY", nullable = false, length = 30)
    private String birthday;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 80)
    @Column(name = "ACCOUNTLINK", nullable = false, length = 80)
    private String accountlink;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "LOCALE", nullable = false, length = 10)
    private String locale;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 200)
    @Column(name = "URL", nullable = false, length = 200)
    private String url;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 40)
    @Column(name = "UPDATEDTIME", nullable = false, length = 40)
    private String updatedtime;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 5)
    @Column(name = "VERIFIED", nullable = false, length = 5)
    private String verified;

    public Facebook() {
    }

    public Facebook(Integer sysid) {
        this.sysid = sysid;
    }

    public Facebook(Integer sysid, String facebookid, String firstname, String lastname, String gender, String birthday, String accountlink, String locale, String url, String updatedtime, String verified) {
        this.sysid = sysid;
        this.facebookid = facebookid;
        this.firstname = firstname;
        this.lastname = lastname;
        this.gender = gender;
        this.birthday = birthday;
        this.accountlink = accountlink;
        this.locale = locale;
        this.url = url;
        this.updatedtime = updatedtime;
        this.verified = verified;
    }

    public Integer getSysid() {
        return sysid;
    }

    public void setSysid(Integer sysid) {
        this.sysid = sysid;
    }

    public String getFacebookid() {
        return facebookid;
    }

    public void setFacebookid(String facebookid) {
        this.facebookid = facebookid;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getAccountlink() {
        return accountlink;
    }

    public void setAccountlink(String accountlink) {
        this.accountlink = accountlink;
    }

    public String getLocale() {
        return locale;
    }

    public void setLocale(String locale) {
        this.locale = locale;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUpdatedtime() {
        return updatedtime;
    }

    public void setUpdatedtime(String updatedtime) {
        this.updatedtime = updatedtime;
    }

    public String getVerified() {
        return verified;
    }

    public void setVerified(String verified) {
        this.verified = verified;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (sysid != null ? sysid.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Facebook)) {
            return false;
        }
        Facebook other = (Facebook) object;
        if ((this.sysid == null && other.sysid != null) || (this.sysid != null && !this.sysid.equals(other.sysid))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.filowifi.ac_free_wifi.entities.Facebook[ sysid=" + sysid + " ]";
    }
    
}
