/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.filowifi.ac_free_wifi.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author null
 */
@Entity
@Table(name = "GOOGLE", catalog = "AC_FREE_WIFI_USERS", schema = "")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Google.findAll", query = "SELECT g FROM Google g")
    , @NamedQuery(name = "Google.findBySysid", query = "SELECT g FROM Google g WHERE g.sysid = :sysid")
    , @NamedQuery(name = "Google.findByGoogleid", query = "SELECT g FROM Google g WHERE g.googleid = :googleid")
    , @NamedQuery(name = "Google.findByFullname", query = "SELECT g FROM Google g WHERE g.fullname = :fullname")
    , @NamedQuery(name = "Google.findByGivenname", query = "SELECT g FROM Google g WHERE g.givenname = :givenname")
    , @NamedQuery(name = "Google.findByFamilyname", query = "SELECT g FROM Google g WHERE g.familyname = :familyname")
    , @NamedQuery(name = "Google.findByImageurl", query = "SELECT g FROM Google g WHERE g.imageurl = :imageurl")
    , @NamedQuery(name = "Google.findByEmail", query = "SELECT g FROM Google g WHERE g.email = :email")})
public class Google implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "SYSID", nullable = false)
    private Integer sysid;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "GOOGLEID", nullable = false, length = 50)
    private String googleid;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "FULLNAME", nullable = false, length = 50)
    private String fullname;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "GIVENNAME", nullable = false, length = 30)
    private String givenname;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "FAMILYNAME", nullable = false, length = 30)
    private String familyname;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 120)
    @Column(name = "IMAGEURL", nullable = false, length = 120)
    private String imageurl;
    // @Pattern(regexp="[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?", message="Invalid email")//if the field contains email address consider using this annotation to enforce field validation
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 80)
    @Column(name = "EMAIL", nullable = false, length = 80)
    private String email;

    public Google() {
    }

    public Google(Integer sysid) {
        this.sysid = sysid;
    }

    public Google(Integer sysid, String googleid, String fullname, String givenname, String familyname, String imageurl, String email) {
        this.sysid = sysid;
        this.googleid = googleid;
        this.fullname = fullname;
        this.givenname = givenname;
        this.familyname = familyname;
        this.imageurl = imageurl;
        this.email = email;
    }

    public Integer getSysid() {
        return sysid;
    }

    public void setSysid(Integer sysid) {
        this.sysid = sysid;
    }

    public String getGoogleid() {
        return googleid;
    }

    public void setGoogleid(String googleid) {
        this.googleid = googleid;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getGivenname() {
        return givenname;
    }

    public void setGivenname(String givenname) {
        this.givenname = givenname;
    }

    public String getFamilyname() {
        return familyname;
    }

    public void setFamilyname(String familyname) {
        this.familyname = familyname;
    }

    public String getImageurl() {
        return imageurl;
    }

    public void setImageurl(String imageurl) {
        this.imageurl = imageurl;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (sysid != null ? sysid.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Google)) {
            return false;
        }
        Google other = (Google) object;
        if ((this.sysid == null && other.sysid != null) || (this.sysid != null && !this.sysid.equals(other.sysid))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.filowifi.ac_free_wifi.entities.Google[ sysid=" + sysid + " ]";
    }

  
    
}
