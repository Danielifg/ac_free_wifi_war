/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.filowifi.ac_free_wifi.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author null
 */
@Entity
@Table(name = "FORMDATA", catalog = "AC_FREE_WIFI_USERS", schema = "")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Formdata.findAll", query = "SELECT f FROM Formdata f")
    , @NamedQuery(name = "Formdata.findBySysid", query = "SELECT f FROM Formdata f WHERE f.sysid = :sysid")
    , @NamedQuery(name = "Formdata.findByUsername", query = "SELECT f FROM Formdata f WHERE f.username = :username")
    , @NamedQuery(name = "Formdata.findByEmail", query = "SELECT f FROM Formdata f WHERE f.email = :email")
    , @NamedQuery(name = "Formdata.findByAge", query = "SELECT f FROM Formdata f WHERE f.age = :age")
    , @NamedQuery(name = "Formdata.findByAcceptsEmails", query = "SELECT f FROM Formdata f WHERE f.acceptsEmails = :acceptsEmails")})
public class Formdata implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "SYSID", nullable = false)
    private Integer sysid;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 25)
    @Column(name = "USERNAME", nullable = false, length = 25)
    private String username;
    // @Pattern(regexp="[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?", message="Invalid email")//if the field contains email address consider using this annotation to enforce field validation
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "EMAIL", nullable = false, length = 30)
    private String email;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 15)
    @Column(name = "AGE", nullable = false, length = 15)
    private String age;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 15)
    @Column(name = "ACCEPTS_EMAILS", nullable = false, length = 15)
    private String acceptsEmails;

    public Formdata() {
    }

    public Formdata(Integer sysid) {
        this.sysid = sysid;
    }

    public Formdata(Integer sysid, String username, String email, String age, String acceptsEmails) {
        this.sysid = sysid;
        this.username = username;
        this.email = email;
        this.age = age;
        this.acceptsEmails = acceptsEmails;
    }

    public Integer getSysid() {
        return sysid;
    }

    public void setSysid(Integer sysid) {
        this.sysid = sysid;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getAcceptsEmails() {
        return acceptsEmails;
    }

    public void setAcceptsEmails(String acceptsEmails) {
        this.acceptsEmails = acceptsEmails;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (sysid != null ? sysid.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Formdata)) {
            return false;
        }
        Formdata other = (Formdata) object;
        if ((this.sysid == null && other.sysid != null) || (this.sysid != null && !this.sysid.equals(other.sysid))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.filowifi.ac_free_wifi.entities.Formdata[ sysid=" + sysid + " ]";
    }
    
}
