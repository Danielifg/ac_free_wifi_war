/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.filowifi.ac_free_wifi.domainControl_view;

import com.filowifi.ac_free_wifi.controller.jpaControl.GoogleJpaController;
import com.filowifi.ac_free_wifi.entities.Google;
import java.io.Serializable;
import java.util.List;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author Daniel
 */

@Named("google")
@RequestScoped
public class googleTable implements Serializable{
          @Inject
        private GoogleJpaController jpa;
        
        private List<Google> dataTable;    

        public List<Google> getDataTable() {
                return jpa.findGoogleEntities();
        }

        public void setDataTable(List<Google> dataTable) {
                this.dataTable = dataTable;
        }
}
