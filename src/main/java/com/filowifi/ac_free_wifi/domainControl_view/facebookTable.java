/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.filowifi.ac_free_wifi.domainControl_view;

import com.filowifi.ac_free_wifi.controller.jpaControl.FacebookJpaController;
import com.filowifi.ac_free_wifi.entities.Facebook;
import java.io.Serializable;
import java.util.List;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author Daniel
 */
@Named("facebook")
@RequestScoped
public class facebookTable implements Serializable {
        
          @Inject
        private FacebookJpaController jpa;
        
        private List<Facebook> dataTable;    

        public List<Facebook> getDataTable() {
                return jpa.findFacebookEntities();
        }

        public void setDataTable(List<Facebook> dataTable) {
                this.dataTable = dataTable;
        }
}
