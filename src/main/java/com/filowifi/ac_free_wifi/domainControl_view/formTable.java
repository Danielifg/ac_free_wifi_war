/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.filowifi.ac_free_wifi.domainControl_view;


import com.filowifi.ac_free_wifi.controller.jpaControl.FormdataJpaController;
import com.filowifi.ac_free_wifi.entities.Formdata;



import java.io.Serializable;
import java.util.List;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;

import javax.inject.Named;

/**
 *
 * @author Daniel
 */
@Named("form")
@RequestScoped
public class formTable implements Serializable {

       @Inject
        private FormdataJpaController jpa;
        
        private List<Formdata> dataTable;    

        public List<Formdata> getDataTable() {
                return jpa.findFormdataEntities();
        }

        public void setDataTable(List<Formdata> dataTable) {
                this.dataTable = dataTable;
        }
       

      

}
