# Use the AWS Elastic Beanstalk GlassFish image
FROM amazon/aws-eb-glassfish:4.1-jdk8-onbuild-3.5.1

EXPOSE 8080 3306

# Install MySQL dependencies
RUN curl -L -o /usr/local/glassfish4/glassfish/domains/domain1/lib/mysql-connector-java-5.1.35.jar https://repo1.maven.org/maven2/mysql/mysql-connector-java/5.1.35/mysql-connector-java-5.1.35.jar

RUN /usr/local/glassfish4/bin/asadmin restart-domain